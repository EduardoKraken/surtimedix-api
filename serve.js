// IMPORTAR DEPENDENCIAS
const express    = require("express");
const bodyParser = require("body-parser");
var   cors       = require('cors')
const fileUpload = require('express-fileupload')

// IMPORTAR EXPRESS
const app = express();

// Rutas estaticas
app.use('/fotos', express.static('../fotos'));
app.use('/pdfs', express.static('../pdf'));

// IMPORTAR PERMISOS
app.use(cors());
// parse requests of content-type: application/json
app.use(bodyParser.json());
// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileUpload()); //subir archivos


// ----IMPORTAR RUTAS---------------------------------------->
var r_users          = require('./routes/users.routes');
var r_articulos      = require('./routes/articulos.routes');
var r_laboratorios   = require('./routes/laboratorios.routes');
var r_clientes       = require('./routes/clientes.routes');
var r_correos        = require('./routes/correos.routes');
var r_entradas       = require('./routes/entradas.routes');
var r_almacen        = require('./routes/almacen.routes');
var r_salidas        = require('./routes/salidas.routes');
var r_existencias    = require('./routes/existencias.routes');
var r_documentos     = require('./routes/documentos.routes');
var r_banners        = require('./routes/banners.routes');
var r_configuracion  = require('./routes/configuracion.routes');
var r_paypal         = require('./routes/paypal.routes');
var r_direcciones    = require('./routes/direcciones.routes');
var r_datosfiscales  = require('./routes/datosfiscales.routes');
var r_ordenes        = require('./routes/ordenes.routes');

r_users(app);
r_articulos(app);
r_laboratorios(app);
r_clientes(app);
r_correos(app);
r_entradas(app);
r_almacen(app);
r_salidas(app);
r_existencias(app);
r_documentos(app);
r_banners(app);
r_configuracion(app);
r_paypal(app);
r_direcciones(app);
r_datosfiscales(app);
r_ordenes(app);
// ----FIN-DE-LAS-RUTAS-------------------------------------->

// DEFINIT PUERTO EN EL QUE SE ESCUCHARA
app.listen(3001, () => {
  console.log("|****************** S-O-F-S-O-L-U-T-I-O-N *********************| ");
  console.log("|******************** C-R-E-A-T-I-B-E *************************| ");
  console.log("|**************************************************************| ");
  console.log("|************ Servidor Corriendo en el Puerto 3001 ************| ");
});