
const Laboratorio = require("../models/laboratorios.model.js");

// Agregar un laboratorio
exports.addLaboratorio = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Laboratorio.addLaboratorio(req.body, (err, data)=>{
  	// EVALUO QUE NO EXISTA UN ERROR
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el Laboratorio"
  		})
  	else res.send(data)

  })
};

// Traer laboratorios por id
exports.getLabId = (req, res)=>{
    Laboratorio.getLabId(req.body,(err,data)=>{
			if(err)
				res.status(500).send({
					message:
						err.message || "Se produjo algún erro al recuperar los grupos por escuelas."
				});
				else res.send(data);
    });
};

// Traer laboratorios activos
exports.getLabActivos = (req,res) => {
    Laboratorio.getLabActivos((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los grupos"
        });
      else res.send(data);
    });
};

// Traer laboratorios 
exports.getLabList = (req,res) => {
    Laboratorio.getLabList((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los grupos"
        });
      else res.send(data);
    });
};

// Actualizar un laboratorio
exports.updateLab = (req, res) =>{
	if (!req.body) {
		res.status(400).send({
		  message: "El Contenido no puede estar vacio!"
		});
	}
	
	Laboratorio.updateLab(req.params.idlaboratorios, req.body ,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre el laboratorio con el id ${req.params.idlaboratorios }.`
				});
			} else {
				res.status(500).send({
				message: "Error al actualizar el laboratorio con el id" + req.params.idlaboratorios 
				});
			}
		} 
		else res.send(data);
	}
	);
}







