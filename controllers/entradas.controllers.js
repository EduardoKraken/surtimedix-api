const Entradas = require("../models/entradas.model.js");

// traer las entradas por fecha
exports.getEntradasFecha = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  Entradas.getEntradasFecha(req.body, (err, data)=>{
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el cliente"
  		})
  	else res.send(data)
  })
};

// agregar una entrada
exports.addEntradas = (req, res) => {
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  Entradas.addEntradas(req.body, (err, data)=>{
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el cliente"
  		})
  	else res.send(data)
  })
};

exports.getEntradasAll =  (req,res)=>{
  Entradas.getEntradasAll((err, data) => {
    if (err){
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los grupos"
      });
    }else{
    	res.send(data)
    } 
  });
};

exports.updateEntrada = (req, res) =>{
 if (!req.body) {
   res.status(400).send({
     message: "El Contenido no puede estar vacio!"
   });
 }
  
 Entradas.updateEntrada(req.params.id, req.body ,(err, data) => {
   if (err) {
     if (err.kind === "not_found") {
       res.status(404).send({
       message: `No encontre el almacen con el id ${req.params.id }.`
       });
     } else {
       res.status(500).send({
       message: "Error al actualizar el almacen con el id" + req.params.id 
       });
     }
   } 
   else res.send(data);
 }
 );
};






