
const Direcciones = require("../models/direcciones.model.js");

// Agregar un laboratorio
exports.addDireccion = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Direcciones.addDireccion(req.body, (err, data)=>{
  	// EVALUO QUE NO EXISTA UN ERROR
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el Laboratorio"
  		})
  	else res.send(data)

  })
};

// Traer laboratorios por id
exports.getDireccionesUsuario = (req, res)=>{
    Direcciones.getDireccionesUsuario(req.params.id,(err,data)=>{
			if(err)
				res.status(500).send({
					message:
						err.message || "Se produjo algún erro al recuperar los grupos por escuelas."
				});
				else res.send(data);
    });
};


// Traer laboratorios por id
exports.getDirecciones = (req, res)=>{
    Direcciones.getDirecciones(req.params.id,(err,data)=>{
			if(err)
				res.status(500).send({
					message:
						err.message || "Se produjo algún erro al recuperar los grupos por escuelas."
				});
				else res.send(data);
    });
};


exports.updateDireccion = (req, res) =>{
	if (!req.body) {
		res.status(400).send({
		  message: "El Contenido no puede estar vacio!"
		});
	}
	
	Direcciones.updateDireccion(req.body ,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre la direccion con el id ${req.body.iddirecciones }.`
				});
			} else {
				res.status(500).send({
				message: "Error al actualizar la direccion con el id" + req.body.iddirecciones 
				});
			}
		} 
		else res.send(data);
	});
};



// Delete a users with the specified usersId in the request
exports.deleteDireccion = (req, res) => {
  Direcciones.deleteDireccion(req.params.id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Users with id ${req.params.id}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el usuario con el id " + req.params.id
        });
      }
    } else res.send({ message: `El usuario se elimino correctamente!` });
  });
};