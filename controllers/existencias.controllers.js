const Existencias = require("../models/existencias.model.js");

exports.getExistencias =  (req,res)=>{
  Existencias.getExistencias((err, data) => {
    if (err){
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los grupos"
      });
    }else{
    	res.send(data)
    } 
  });
};


// agregar una entrada
exports.getExistenciaCodigo = (req, res) => {
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  Existencias.getExistenciaCodigo(req.params.codigo, (err, data)=>{
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el cliente"
  		})
  	else res.send(data)
  })
};


// // traer las entradas por fecha
exports.getExistenciaTotal = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  Existencias.getExistenciaTotal((err, data)=>{
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el cliente"
  		})
  	else res.send(data)
  })
};

