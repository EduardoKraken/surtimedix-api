
const Datosfiscales = require("../models/datosfiscales.model.js");

// Agregar un laboratorio
exports.addDatosfiscales = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Datosfiscales.addDatosfiscales(req.body, (err, data)=>{
  	// EVALUO QUE NO EXISTA UN ERROR
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el CLiente"
  		})
  	else res.send(data)

  })
};


// Traer laboratorios por id
exports.getDatosfiscales = (req, res)=>{
    Datosfiscales.getDatosfiscales(req.params.id, (err,data)=>{
			if(err)
				res.status(500).send({
					message:
						err.message || "Se produjo algún erro al recuperar los grupos por escuelas."
				});
				else res.send(data);
    });
};


exports.updateDatosfiscales = (req, res) =>{
	if (!req.body) {
		res.status(400).send({
		  message: "El Contenido no puede estar vacio!"
		});
	}
	
	Datosfiscales.updateDatosfiscales(req.body,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre la direccion con el id ${req.body }.`
				});
			} else {
				res.status(500).send({
				message: "Error al actualizar la direccion con el id" + req.body 
				});
			}
		} 
		else res.send(data);
	});
};

// /////////////////////// Datos de la tienda ////////////////////
// Traer laboratorios por id
exports.getDatosfiscalesTienda = (req, res)=>{
    Datosfiscales.getDatosfiscalesTienda((err,data)=>{
			if(err)
				res.status(500).send({
					message:
						err.message || "Se produjo algún erro al recuperar los grupos por escuelas."
				});
				else res.send(data);
    });
};

exports.updateDatosfiscalesTienda = (req, res) =>{
	if (!req.body) {
		res.status(400).send({
		  message: "El Contenido no puede estar vacio!"
		});
	}
	
	Datosfiscales.updateDatosfiscalesTienda(req.body,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre la direccion con el id ${req.body }.`
				});
			} else {
				res.status(500).send({
				message: "Error al actualizar la direccion con el id" + req.body 
				});
			}
		} 
		else res.send(data);
	});
};

