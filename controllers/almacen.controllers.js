const Almacen = require("../models/almacen.model.js");

// agregar una entrada
exports.addAlmacen = (req, res) => {
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  Almacen.addAlmacen(req.body, (err, data)=>{
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el cliente"
  		})
  	else res.send(data)
  })
};

exports.getAlmacenList =  (req,res)=>{
  Almacen.getAlmacenList((err, data) => {
    if (err){
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los grupos"
      });
    }else{
    	res.send(data)
    } 
  });
};


exports.updateAlmacen = (req, res) =>{
  if (!req.body) {
   res.status(400).send({
     message: "El Contenido no puede estar vacio!"
   });
  }
  
 Almacen.updateAlmacen(req.params.id, req.body ,(err, data) => {
   if (err) {
     if (err.kind === "not_found") {
       res.status(404).send({
       message: `No encontre el almacen con el id ${req.params.id }.`
       });
     } else {
       res.status(500).send({
       message: "Error al actualizar el almacen con el id" + req.params.id 
       });
     }
   } 
   else res.send(data);
 });
};








