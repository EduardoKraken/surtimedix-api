const Ordenes = require("../models/ordenes.model.js");

// agregar una entrada
exports.addOrden = (req, res) => {
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  Ordenes.addOrden(req.body, (err, data)=>{
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear la orden"
  		})
  	else res.send(data)
  })
};


// traer las entradas por fecha
exports.getOrdenesUsuario = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  Ordenes.getOrdenesUsuario(req.params.id, (err, data)=>{
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el cliente"
  		})
  	else res.send(data)
  })
};


exports.getOrdenes =  (req,res)=>{
  Ordenes.getOrdenes((err, data) => {
    if (err){
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar las ordenes"
      });
    }else{
    	res.send(data)
    } 
  });
};

exports.getOrdenId =  (req,res)=>{
  Ordenes.getOrdenId(req.params.id,(err, docum) => {
    if (err){
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar las ordenes"
      });
    }else{
    	Ordenes.getMovim(req.params.id,(err, movim) => {
		    if (err){
		      res.status(500).send({
		        message:
		          err.message || "Se produjo algún error al recuperar las ordenes"
		      });
		    }else{
		    	console.log('docum',docum)
		    	var respuesta = {
					  iddocum         :docum[0].iddocum,
					  idusuariosweb   :docum[0].idusuariosweb,
					  direccion       :docum[0].direccion,
					  importe         :docum[0].importe,
					  descuento       :docum[0].descuento,
					  subtotal        :docum[0].subtotal,
					  total           :docum[0].total,
					  iva             :docum[0].iva,
					  folio           :docum[0].folio,
					  estatus         :docum[0].estatus,
					  nota            :docum[0].nota,
					  divisa          :docum[0].divisa,
					  hora            :docum[0].hora,
					  fecha           :docum[0].fecha,
					  fechapago       :docum[0].fechapago,
					  refer           :docum[0].refer,
					  tipodoc         :docum[0].tipodoc,
					  movim           :movim
					}

					console.log('respuest',respuesta)

		    	res.send(respuesta)
		    }
		  })
    } 
  });
};

exports.updateOrden = (req, res) =>{
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }
  
 Ordenes.updateOrden(req.params.id, req.body ,(err, data) => {
   if (err) {
     if (err.kind === "not_found") {
       res.status(404).send({
       message: `No encontre el almacen con el id ${req.params.id }.`
       });
     } else {
       res.status(500).send({
       message: "Error al actualizar el almacen con el id" + req.params.id 
       });
     }
   } 
   else res.send(data);
 });
};






