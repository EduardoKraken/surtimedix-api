const sql = require("./db.js");

// constructor
const Laboratorio = function(lab) {
    this.idlaboratorios   = lab.idlaboratorios;
    this.nomlab           = lab.nomlab;
    this.estatus          = lab.estatus;
};

// Agregar un laboratorio
Laboratorio.addLaboratorio = (newLab, result) => {
	sql.query(`INSERT INTO laboratorios(nomlab,estatus)VALUES(?,?)`,
						 [newLab.nomlab,newLab.estatus], (err, res) => {	
    if (err) {
    console.log("error: ", err);
    result(err, null);
    return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear Laboratorio: ", { id: res.insertId, ...newLab });
    result(null, { id: res.insertId, ...newLab });
	});
};

// Traer laboratorios por id
Laboratorio.getLabId = (params, result)=>{
	sql.query("SELECT * FROM laboratorios WHERE idlaboratorios=? ", [params.idlaboratorios], (err,res)=>{
		if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res);
	})
};

// Traer laboratior activos
Laboratorio.getLabActivos = result => {
  sql.query(`SELECT * FROM laboratorios WHERE estatus = 1`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("laboratorios: ", res);
    result(null, res);
  });
};

// Traer laboratior activos
Laboratorio.getLabList = result => {
  sql.query(`SELECT * FROM laboratorios`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("laboratorios: ", res);
    result(null, res);
  });
};

// Actualizar laboratorios
Laboratorio.updateLab = (id, lab, result) => {
  sql.query(` UPDATE laboratorios SET nomlab = ?, estatus = ?
                WHERE idlaboratorios = ?`, [lab.nomlab, lab.estatus, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated lab: ", { id: id, ...lab });
      result(null, { id: id, ...lab });
    }
  );
};


module.exports = Laboratorio;