const sql = require("./db.js");

// constructor

const Alamacen = almacen => {
  this.codigo      = almacen.codigo;
  this.lote        = almacen.lote;
  this.cant        = almacen.cant;
  this.caducidad   = almacen.caducidad;
};


Alamacen.addAlmacen = (ent, result) => {
  sql.query(`INSERT INTO almacen (codigo,lote,cant,caducidad)
                                  VALUES(?,?,?,?)`,
    [ent.codigo,ent.lote,ent.cant,ent.caducidad], (err, res) => {  
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear almacen: ", { id: res.insertId, ...ent });
    result(null, { id: res.insertId, ...ent });
  });
};

Alamacen.getAlmacenList = result => {
  sql.query(`SELECT * FROM almacen;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("almacen: ", res);
    result(null, res);
  });
};

Alamacen.updateAlmacen = (id, alm, result) => {
  sql.query(`UPDATE almacen SET cant= ? WHERE idalmacen= ? `, [alm.cant, id],(err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("updated almacen: ", { id: id, ...alm });
    result(null, { id: id, ...alm });
  });
};

module.exports = Alamacen;