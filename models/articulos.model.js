const sql = require("./db.js");

// constructor

const Articulos = articulos => {
    this.id             = articulos.id;
    this.nomart         = articulos.nomart;
    this.codigo         = articulos.codigo;
    this.idlaboratorio  = articulos.idlaboratorio;
    this.nomlab         = articulos.nomlab;
    this.descrip        = articulos.descrip;
    this.statusweb      = articulos.statusweb;
    this.precio1        = articulos.precio1;
    this.sal            = articulos.sal;
    this.novedades      = articulos.novedades;
    this.destacados     = articulos.destacados;
    this.pjedesc         = articulos.pjedesc;
}


Articulos.getAll = result => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.sal, a.idlaboratorio, l.nomlab, a.descrip, a.statusweb, a.precio1, a.pjedesc
  	FROM arts a LEFT JOIN laboratorios l ON l.idlaboratorios = a.idlaboratorio;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res);
    result(null, res);
  });
};

Articulos.getActivosArts = result => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.sal, a.idlaboratorio, l.nomlab, a.descrip, a.statusweb, a.precio1, a.novedades, a.destacados, a.pjedesc
    FROM arts a LEFT JOIN laboratorios l ON l.idlaboratorios = a.idlaboratorio WHERE statusweb = 1;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res);
    result(null, res);
  });
};

Articulos.getNovedades = result => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.sal, a.idlaboratorio, l.nomlab, a.descrip, a.statusweb, a.precio1, a.novedades, 
    a.destacados, a.pjedesc FROM arts a LEFT JOIN laboratorios l ON l.idlaboratorios = a.idlaboratorio WHERE a.novedades = 1 AND a.statusweb = 1;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res);
    result(null, res);
  });
};

Articulos.getDestacados = result => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.sal, a.idlaboratorio, l.nomlab, a.descrip, a.statusweb, a.precio1, a.novedades, 
    a.destacados, a.pjedesc FROM arts a LEFT JOIN laboratorios l ON l.idlaboratorios = a.idlaboratorio WHERE a.destacados = 1 AND a.statusweb = 1;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res);
    result(null, res);
  });
};

Articulos.getArtxLab =(id, result) => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.sal, a.idlaboratorio, l.nomlab, a.descrip, a.statusweb, a.precio1, a.pjedesc
    FROM arts a LEFT JOIN laboratorios l ON l.idlaboratorios = a.idlaboratorio 
    WHERE  a.statusweb = 1 AND a.idlaboratorio = ?;`,[id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res);
    result(null, res);
  });
};

Articulos.getPromociones = result => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.sal, a.idlaboratorio, l.nomlab, a.descrip, a.statusweb, a.precio1, a.pjedesc
    FROM arts a LEFT JOIN laboratorios l ON l.idlaboratorios = a.idlaboratorio WHERE a.pjedesc > 0 AND a.statusweb = 1;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res);
    result(null, res);
  });
};


Articulos.fotosArt = result => {
  sql.query(`SELECT idfotos, codigo, image_name, principal FROM fotos`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res);
    result(null, res);
  });
};

Articulos.fotosxArt = (codigo, result) => {
  sql.query(`SELECT idfotos, codigo, image_name, principal FROM fotos WHERE codigo = ?`,[codigo], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res);
    result(null, res);
  });
};

Articulos.addFoto = (foto, result) => {
  sql.query(`INSERT INTO fotos(codigo, image_name, principal)VALUES(?,?,?)`,
    [foto.codigo,foto.image_name,foto.principal], (err, res) => {  
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear Grupo: ", { id: res.insertId, ...foto });
    result(null, { id: res.insertId, ...foto });
  });
};


Articulos.addArticulos = (art, result) => {
  sql.query(`INSERT INTO arts(nomart,codigo,sal,idlaboratorio,descrip)VALUES(?,?,?,?,?)`,
    [art.nomart,art.codigo,art.sal,art.idlaboratorio,art.descrip], (err, res) => {  
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear Grupo: ", { id: res.insertId, ...art });
    result(null, { id: res.insertId, ...art });
  });
};


Articulos.getArticuloCodigo = (codigo, result) => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.sal, a.idlaboratorio, l.nomlab, a.descrip, a.statusweb, a.precio1, a.pjedesc
    FROM arts a LEFT JOIN laboratorios l ON l.idlaboratorios = a.idlaboratorio WHERE codigo = ?;`,[ codigo ], (err, res) => {
    if (err) {
      console.log("error: ", err); 
      result(err, null);
      return;
    }

    console.log("found students: ", res);
    result(null, res);
  });
};


Articulos.getArticuloTienda = (codigo, result) => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.sal, a.idlaboratorio, l.nomlab, a.descrip, a.statusweb, a.precio1, a.novedades, a.destacados, a.pjedesc
    FROM arts a LEFT JOIN laboratorios l ON l.idlaboratorios = a.idlaboratorio WHERE codigo = ?;`,[ codigo ], (err, res) => {
    if (err) {
      console.log("error: ", err); 
      result(err, null);
      return;
    }

    console.log("found students: ", res);
    result(null, res);
  });
};

Articulos.updateArticulosMovil = (id, art, result) => {
  sql.query(` UPDATE arts SET nomart = ?, idlaboratorio = ?, sal =?
                WHERE id = ?`, [art.nomart, art.idlaboratorio, art.sal,  art.statusweb,  id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated articulos: ", { id: id, ...art });
      result(null, { id: id, ...art });
    }
  );
};


Articulos.updateArticulos = (id, art, result) => {
  sql.query(` UPDATE arts SET nomart = ?, idlaboratorio = ?, sal = ?, statusweb = ?, descrip = ?, precio1 = ?, pjedesc = ?
                WHERE id = ?;`, [art.nomart, art.idlaboratorio, art.sal,art.statusweb,art.descrip,art.precio1,art.pjedesc,  id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated articulos: ", { id: id, ...art });
      result(null, { id: id, ...art });
    }
  );
};

Articulos.updateNovedades = (id, art, result) => {
  sql.query(` UPDATE arts SET novedades = ?, destacados = ? WHERE id = ?;`, [art.novedades,art.destacados, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated articulos: ", { id: id, ...art });
      result(null, { id: id, ...art });
    }
  );
};

Articulos.deleteFoto = (id, result) => {
  sql.query("DELETE FROM fotos where idfotos = ?;",[id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.affectedRows} banners`);
    result(null, res);
  });
};




module.exports = Articulos;