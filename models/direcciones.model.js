const sql = require("./db.js");

// constructor
const Direcciones = function(lab) {
    this.iddirecciones    = lab.iddirecciones;
    this.nomdir           = lab.nomdir;
    this.calle            = lab.calle;
    this.numext           = lab.numext;
    this.numint           = lab.numint;
    this.colonia          = lab.colonia;
    this.ciudad           = lab.ciudad;
    this.estado           = lab.estado;
    this.telefono         = lab.telefono;
    this.estatus          = lab.estatus;
    this.idusuariosweb    = lab.idusuariosweb;
    this.cp               = lab.cp;
};

// Agregar un laboratorio
Direcciones.addDireccion = (d, result) => {
	sql.query(`INSERT INTO direcciones(iddirecciones,nomdir,calle,numext,numint,colonia,ciudad,estado,telefono,estatus,idusuariosweb,cp)
		VALUES(?,?,?,?,?,?,?,?,?,?,?,?)`,[d.iddirecciones,d.nomdir,d.calle,d.numext,d.numint,d.colonia,d.ciudad,d.estado,
		d.telefono,d.estatus,d.idusuariosweb,d.cp], (err, res) => {	
    if (err) {
    console.log("error: ", err);
    result(err, null);
    return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear Direcciones: ", { id: res.insertId, ...d });
    result(null, { id: res.insertId, ...d });
	});
};

// Traer laboratorios por id
Direcciones.getDireccionesUsuario = (id, result)=>{
	sql.query(`SELECT iddirecciones, nomdir, calle, numint, numext, colonia, estado, ciudad, estatus, idusuariosweb, cp, CONCAT(calle,', ', numint,', ', numext,', ', colonia,', ', ciudad,', ', estado,', ',cp ) AS "direccion" FROM direcciones WHERE idusuariosweb=? `, [id], (err,res)=>{
		if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res);
	})
};

// Traer laboratorios por id
Direcciones.getDirecciones = (id, result)=>{
	sql.query(`SELECT iddirecciones, nomdir, calle, numint, numext, colonia, estado, ciudad, estatus, idusuariosweb, cp, CONCAT(calle,', ', numint,', ', numext,', ', colonia,', ', ciudad,', ', estado,', ',cp ) AS "direccion" FROM direcciones WHERE iddirecciones=? `, [id], (err,res)=>{
		if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res);
	})
};

// Actualizar laboratorios
Direcciones.updateDireccion = (d, result) => {
  sql.query(` UPDATE direcciones SET nomdir = ?,calle = ?,numext = ?,numint = ?,
  	colonia = ?,ciudad = ?,estado = ?,telefono = ?,estatus = ?, cp = ?
                WHERE iddirecciones = ?`, [d.nomdir, d.calle ,d.numext,d.numint,d.colonia,d.ciudad,d.estado,d.telefono,d.estatus,d.cp,d.iddirecciones],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated lab: ", { id: d });
      result(null, { id: d });
    }
  );
};

Direcciones.deleteDireccion = (id, result) => {
  sql.query("DELETE FROM direcciones WHERE iddirecciones = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted direcciones with id: ", id);
    result(null, res);
  });
};


module.exports = Direcciones;