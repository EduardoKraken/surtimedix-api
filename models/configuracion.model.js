const sql = require("./db.js");

// constructor
const Configuracion = function(ciaws) {
    this.id         = ciaws.id;
    this.nomcia   = ciaws.nomcia;
    this.logourl    = ciaws.logourl;
    this.email1    = ciaws.email1;
    this.email2    = ciaws.email2;
    this.condicom    = ciaws.condicom;
    this.facebook    = ciaws.facebook;
    this.nosotros    = ciaws.nosotros;
    this.cuentas    = ciaws.cuentas;
    this.primario    = ciaws.primario;
    this.secundario    = ciaws.secundario;
    this.informatico    = ciaws.informatico;
    this.correcto    = ciaws.correcto;
    this.errores    = ciaws.errores;
    this.peligro    = ciaws.peligro;
};



// Traer laboratior activos
Configuracion.getConfiguracion = result => {
  sql.query(`SELECT * FROM ciaws`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("laboratorios: ", res);
    result(null, res);
  });
};

// Actualizar laboratorios
Configuracion.updateConfig = (ciaws, result) => {
  sql.query(` UPDATE ciaws SET email1 = ?, email2 = ?, nomcia = ?, logourl = ?, condicom = ?, 
    facebook = ?, nosotros = ?, cuentas = ?, primario = ?, secundario = ?, informatico = ?, correcto = ?, errores = ?, peligro = ?
    WHERE id = 1`, [ciaws.email1, ciaws.email2, ciaws.nomcia, ciaws.logourl, ciaws.condicom, ciaws.facebook, ciaws.nosotros, ciaws.cuentas, 
  	ciaws.primario, ciaws.secundario, ciaws.informatico, ciaws.correcto, ciaws.errores, ciaws.peligro],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated lab: ", { id:  ciaws });
      result(null, { id:  ciaws });
    }
  );
};


module.exports = Configuracion;