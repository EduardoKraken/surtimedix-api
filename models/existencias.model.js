const sql = require("./db.js");

// constructor

const Existencias = existencias => {
  this.idexistencias  = existencias.idexistencias;
  this.nomart         = existencias.nomart;
  this.codigo         = existencias.codigo;
  this.lote           = existencias.lote;
  this.cant           = existencias.cant;
  this.caducidad      = existencias.caducidad;
  this.suma           = existencias.suma;
  this.lab            = existencias.lab;
  this.sal            = existencias.sal;
};



Existencias.getExistencias = result => {
  sql.query(`SELECT ar.nomart, a.codigo, l.nomlab AS "lab", ar.sal, sum(a.cant) AS "suma"         
FROM almacen a LEFT JOIN arts ar ON a.codigo = ar.codigo LEFT JOIN laboratorios l          
ON l.idlaboratorios = ar.idlaboratorio GROUP BY  ar.nomart, a.codigo, l.nomlab, ar.sal HAVING sum(a.cant) > 0;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("entradas: ", res);
    result(null, res);
  });
};

Existencias.getExistenciaCodigo =(codigo, result) => {
  sql.query(`SELECT ar.nomart, a.idalmacen, a.codigo, a.lote, a.caducidad, a.cant, ar.sal, l.nomlab AS "lab"
        FROM almacen a INNER JOIN arts ar ON a.codigo=ar.codigo INNER JOIN laboratorios l ON 
        l.idlaboratorios = ar.idlaboratorio WHERE a.codigo = ? AND a.cant > 0
        ORDER BY a.caducidad ASC, a.cant DESC ;`,[codigo], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("entradas: ", res);
    result(null, res);
  });
};


Existencias.getExistenciaTotal = result => {
  sql.query(`SELECT ar.nomart, a.idalmacen, a.codigo, a.lote, a.caducidad, a.cant, ar.sal, l.nomlab AS "lab"
    FROM almacen  a INNER JOIN arts ar ON a.codigo=ar.codigo INNER JOIN laboratorios l ON 
    l.idlaboratorios = ar.idlaboratorio WHERE a.cant > 0 ORDER BY a.caducidad ASC, a.cant DESC;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("entradas: ", res);
    result(null, res);
  });
};

module.exports = Existencias;