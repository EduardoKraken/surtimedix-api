module.exports = app => {
    const articulos = require('../controllers/articulos.controllers') // --> ADDED THIS

    // Apis generales
    app.get("/articulos.list", articulos.findAll);   // BUSCAR 
    app.post("/articulos.add", articulos.addArticulos);
    app.get("/articulos.codigo/:codigo", articulos.getArticuloCodigo);
    app.get("/articulos.fotos/:codigo", articulos.fotosxArt);
    app.post("/fotos.add", articulos.addFoto);

    // Admin
    app.put("/admin/articulos.update/:id", articulos.updateArticulos);
    app.put("/admin/articulos.novedades/:id", articulos.updateNovedades);
    app.get("/admin/articulos.activos", articulos.getActivosArts);
    app.delete("/admin/foto.art.delete/:id",   articulos.deleteFoto);


    // Tienda
    app.get("/tienda/articulos.novedades.get", articulos.getNovedades);
    app.get("/tienda/articulos.destacados.get", articulos.getDestacados);
    app.get("/tienda/articulos.promociones.get", articulos.getPromociones);
    app.get("/tienda/articulos.laboratorio.get/:id", articulos.getArtxLab);
    app.get("/tienda/articulos.codigo/:codigo", articulos.getArticuloTienda);

    // movil
    app.put("/movil/articulos.update/:id", articulos.updateArticulosMovil);
    app.get("/movil/articulos.list", articulos.getArticulosMovil);   // BUSCAR 

  };