module.exports = app => {
  const ordenes = require('../controllers/ordenes.controllers') // --> ADDED THIS

  // Agregar orden
  app.post("/ordenes.add",  ordenes.addOrden);   // CREAR UN NUEVO GRUPO
  
  // // todas las ordenes
  app.get("/ordenes.list",   ordenes.getOrdenes);   
  app.get("/ordenes.id/:id",   ordenes.getOrdenId);   
  // // por tipo de documento
  app.put("/ordenes.update/:id",   ordenes.updateOrden);   


  // //ordenes de usuario
  // todas las ordenes
  app.get("/ordenes.usuario/:id",   ordenes.getOrdenesUsuario);   
  // // por tipo de documento
  // app.get("/ordenes.usuario.tipodoc/:tipodoc/:id",    ordenes.getOrdenTipodocUsuario); 
  // // Ordenes por usuario 

};