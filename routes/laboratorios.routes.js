module.exports = app => {
  const laboratorios = require('../controllers/laboratorios.controllers') // --> ADDED THIS

  // Rutas para el administrador
  app.post("/admin/laboratorios.add"		            , laboratorios.addLaboratorio);   // BUSCAR 
  app.get("/admin/laboratorios.id"            			, laboratorios.getLabId);   // BUSCAR 
  app.put("/admin/laboratorios.update/:idlaboratorios"	, laboratorios.updateLab);

  // Rutas para la tienda
  app.get("/tienda/laboratorios.activos"				, laboratorios.getLabActivos);   // BUSCAR 

  // Movil
  app.get("/movil/laboratorios.list"				, laboratorios.getLabList);   // BUSCAR 

  // admin
  // app.get("/admin/laboratorios.list"				, laboratorios.getLabList);   // BUSCAR 


};