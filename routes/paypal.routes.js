module.exports = app => {
  const paypal = require('../models/paypal.js') // --> ADDED THIS
  // const paypal2 = require('../models/paypal2.js') // --> ADDED THIS

  app.post("/paypal.send",  paypal.addPaypal);   // CREAR UN NUEVO GRUPO
  app.post("/paypal.order/:id",  paypal.addPaypalOrder);   // CREAR UN NUEVO GRUPO


  // app.post("/paypal.send.sandbox",  paypal2.addPaypal);   // CREAR UN NUEVO GRUPO
  // app.post("/paypal.order.sandbox/:id",  paypal2.addPaypalOrder);   // CREAR UN NUEVO GRUPO

};