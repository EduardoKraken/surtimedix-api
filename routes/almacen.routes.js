module.exports = app => {
  const almacen = require('../controllers/almacen.controllers') // --> ADDED THIS

  // movil
  app.post("/almacen.add",       almacen.addAlmacen);   // BUSCAR
  app.put("/almacen.update/:id", almacen.updateAlmacen);
};