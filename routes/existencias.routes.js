module.exports = app => {
  const existencias = require('../controllers/existencias.controllers') // --> ADDED THIS

  // movil
  app.get("/existencias.list",           existencias.getExistencias);   // BUSCAR
  app.get("/existencias.codigo/:codigo", existencias.getExistenciaCodigo);  
  app.get("/existencias.total",          existencias.getExistenciaTotal);   
};