module.exports = app => {
  const users = require('../controllers/users.controllers') // --> ADDED THIS

  // Iniciar Sesion 
  app.post("/sessions", users.session);

  app.put("/updateuser/:idusuariosweb", users.updatexId);
  // Create a new Users
  app.post("/user.add", users.create);
  // Retrieve all users
  app.get("/users", users.findAll);
  // Retrieve a single Customer with customerId
  app.get("/users/:userId", users.findOne);
  // Traer un usuario por email
  app.post("/user.email", users.getxEmail);
  // Update a Customer with customerId
  app.put("/users/:userId", users.update);
  // Activar usuario
  app.put("/usuario.activar/:idusuario", users.activarUsuario);
  
};