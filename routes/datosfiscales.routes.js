module.exports = app => {
  const datosfiscales = require('../controllers/datosfiscales.controllers') // --> ADDED THIS

  // movil
  app.post("/datosfiscales.add",           datosfiscales.addDatosfiscales);   // BUSCAR
  app.get("/datosfiscales.usuario/:id",    datosfiscales.getDatosfiscales);   // BUSCAR
  app.put("/datosfiscales.update",         datosfiscales.updateDatosfiscales); 

  // Datos fiscales de latienda
  app.get("/tienda/datosfiscales",    			datosfiscales.getDatosfiscalesTienda); 
  app.put("/tienda/datosfiscales.update",       datosfiscales.updateDatosfiscalesTienda); 


}; 