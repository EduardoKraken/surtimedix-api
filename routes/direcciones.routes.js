module.exports = app => {
  const direcciones = require('../controllers/direcciones.controllers') // --> ADDED THIS

  // movil
  app.post("/direcciones.add",           direcciones.addDireccion);   // BUSCAR
  app.get("/direcciones.usuario/:id",    direcciones.getDireccionesUsuario);   // BUSCAR
  app.get("/direcciones.id/:id",         direcciones.getDirecciones);
  app.put("/direcciones.update",         direcciones.updateDireccion);
  app.delete("/direcciones.delete/:id",  direcciones.deleteDireccion);

};