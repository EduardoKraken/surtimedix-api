module.exports = app => {
  const configuracion = require('../controllers/configuracion.controllers') // --> ADDED THIS

  // movil
  app.get("/config.list",       configuracion.getConfiguracion);   // BUSCAR
  app.put("/config.update", configuracion.updateConfig);

};