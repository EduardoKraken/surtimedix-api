module.exports = app => {
  const salidas = require('../controllers/salidas.controllers') // --> ADDED THIS
  // movil
  app.get("/salidas.list",       salidas.getSalidasAll);  
  app.post("/salidas.fecha",     salidas.getSalidasFecha);  
  app.post("/salidas.add",       salidas.addSalidas);

};